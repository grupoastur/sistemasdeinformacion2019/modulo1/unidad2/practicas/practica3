﻿USE practica1;

                           -- PRACTICA TRES --
-- 1 visualizar el número de empleados de cada departamento. Utilizar group by para agrupar por departamento.
SELECT COUNT(*) numemple, e.emp_no numdepart FROM emple e GROUP BY e.dept_no;

-- 2 visualizarr los departamentos con más de 5 empleados. Utilizar group by para agrupar por departamento y 
-- havin para establecer la condición sobre los grupos. 
SELECT e.dept_no numdepart, e.emp_no empleados FROM emple e GROUP BY e.dept_no HAVING e.emp_no>5;

-- 3 hallar la media de los salarios de cada departamento(utilizar la función avg y group by)
  SELECT e.dept_no departamento, e.salario media_salarios FROM emple e GROUP BY e.dept_no HAVING AVG( e.salario);
  
-- 4 visualizar el nombre de los empleados vendedores del departamento ventas (nombre del departamento=ventas, oficio=vendedor.
  SELECT e.apellido FROM depart d JOIN emple e ON d.dept_no = e.dept_no WHERE d.dnombre ='VENTAS' AND e.oficio='VENDEDOR' ;
  
-- 5 visualizar el número de vendedores del departamento ventas (utilizar la función count sobre la consulta anterior)
  SELECT COUNT(*) FROM emple e JOIN depart d ON e.dept_no = d.dept_no WHERE d.dnombre='VENTAS' AND e.oficio='VENDEDOR';
  
-- 6 visualizar los oficios de los empleados del departamento ventas
  SELECT e.oficio FROM emple e INNER JOIN depart d ON e.dept_no = d.dept_no WHERE d.dnombre='VENTAS';
  
-- 7 a partir de la tabla "emple", visualizar el número de empleados de cada departamento cuyo oficio sea "empleado" 
 -- utilizar group by para agrupar por departamento. En la clausula where habrá que indicar que el oficio es "empleado"                 
SELECT COUNT(*), e.dept_no FROM emple e WHERE e.oficio='empleado' GROUP BY e.dept_no;

-- 8 visualizar el departamento con más empleados
  SELECT d.dept_no, COUNT(*) numempleados, d.dnombre FROM depart d JOIN emple e ON d.dept_no = e.dept_no GROUP BY d.dept_no;
  SELECT MAX( c1.dnombre) maximo FROM (SELECT d.dept_no, COUNT(*) numempleados, d.dnombre FROM depart d JOIN emple e 
  ON d.dept_no = e.dept_no GROUP BY d.dept_no) c1;
  
-- 9 mostrar los departamentos cuya suma de salarios sea mayor que la media de salarios de todos los empleados
  SELECT e.dept_no, SUM( e.salario) sumadesalario, AVG( e.salario) mediadesalario FROM emple e GROUP BY e.dept_no 
  HAVING SUM( e.salario)> AVG( e.salario) ;     
               
-- 10 para cada oficio obtener la suma de salarios
  SELECT SUM( e.salario) sumadesalariospor, e.oficio FROM emple e GROUP BY e.oficio;

-- 11 visualizar la suma d salarios de cada oficio del departamento de ventas
 SELECT e.oficio, SUM(e.salario) FROM emple e WHERE e.dept_no=(SELECT d.dept_no FROM depart d WHERE d.dnombre="ventas")  GROUP BY e.oficio;
-- otra forma
 SELECT e.oficio, SUM(e.salario) FROM emple e JOIN depart d ON e.dept_no = d.dept_no 
WHERE d.dnombre="ventas"  GROUP BY e.oficio;
  
-- 12 visualizar el número de departamento que tenga mas empleados cuyo oficio sea empleado
  -- subconsulta
  SELECT COUNT(*), e.dept_no, e.oficio FROM emple e WHERE e.oficio='EMPLEADO' GROUP BY e.dept_no;
  -- consulta final
  SELECT COUNT(*) numempleados, MAX( c1.dept_no) numdepartamento, c1.oficio
  FROM (SELECT COUNT(*), e.dept_no, e.oficio FROM emple e WHERE e.oficio='EMPLEADO' GROUP BY e.dept_no) c1;
  
 -- 13 mostrar el número de oficios distintos de cada departamento
  SELECT COUNT(DISTINCT e.oficio) oficio, e.dept_no departamento FROM emple e GROUP BY e.dept_no;
  
-- 14 mostrar los departamentos que tengan más de dos personas trabajando en la misma profesión
  SELECT e.oficio, e.dept_no, e.emp_no FROM emple e WHERE e.emp_no>2  GROUP BY e.oficio;

-- 15 Dada la tabla herramientas, visualizar por cada estanteria la suma de las unidades
  SELECT h.estanteria, SUM(h.unidades) sumaUnidades FROM herramientas h GROUP BY h.estanteria;
  
-- 16 Visualizar la estantería con más unidades de la tabla herramientas. (con totales y sin totales)
-- subconsulta
  SELECT MAX(c1.sumaUnidades) FROM (SELECT h.estanteria, SUM(h.unidades) sumaUnidades FROM herramientas h GROUP BY h.estanteria) c1;
-- consulta final
  SELECT c1.estanteria FROM (SELECT * FROM herramientas h GROUP BY h.estanteria) c1 
   JOIN (SELECT MAX(c1.sumaUnidades) maximo FROM (SELECT h.estanteria, SUM(h.unidades) sumaUnidades
    FROM herramientas h GROUP BY h.estanteria) c1 ) c2 ON c1.sumaUnidades=c2.maximo;
  
-- 17 Mostrar el número de médicos que pertenecin a cada hospital, ordenado por número descendente de hospital
  SELECT m.cod_hospital, COUNT(*) nmedicos FROM medicos m GROUP BY m.cod_hospital ORDER BY m.cod_hospital DESC;
  
-- 18 Realizar una consulta en la que se muestre por cada hospital el nombre de las especialidades que tiene
  SELECT DISTINCT m.cod_hospital, m.especialidad FROM medicos m ORDER BY m.cod_hospital;
  
-- 19 Realizar una consulta en la que aparezca por cada hospital y en cada especialidad el número de médicos
-- (tendras que partir de la consulta anterior Y utilizar GROUP by) 
  SELECT c1.cod_hospital, c1.especialidad, COUNT(*) nmedicos  FROM 
    (SELECT m.cod_hospital, m.especialidad FROM medicos m) c1 GROUP BY c1.cod_hospital, c1.especialidad;
  
-- 20 Obtener por cada hospital el número de empleados
  SELECT p.cod_hospital, COUNT(*) FROM medicos m JOIN personas p USING(cod_hospital) GROUP BY p.cod_hospital;
  
-- 21 Obtener por cada especialidad el número de trabajadores
  
  
-- 22 Visualizar la especialidad que tenga más médicos
  
  
-- 23 Cual es el nombre del hospital que tiene mayor número de plazas
  -- c1: calcular el número de plazas que hay
  SELECT MAX(h.num_plazas) maximo FROM hospitales h; 
  -- consulta final
  SELECT h.nombre FROM hospitales h WHERE h.num_plazas=(  SELECT MAX(h.num_plazas) maximo FROM hospitales h);
  
-- 24 Visualizar las diferentes estanterias de la tabla herramientas ordenados descendentemente por estanteria
  SELECT h.estanteria, h.descripcion, h.unidades FROM herramientas h ORDER BY h.estanteria DESC;
  
-- 25 Averiguar cuantas unidades tiene cada estantería
  SELECT h.estanteria, SUM(h.unidades) nUnidades FROM herramientas h GROUP BY h.estanteria; 
  
-- 26 Visualizar las estanterias que tengan más de 15 unidades
  SELECT h.estanteria, SUM(h.unidades) nUnidades FROM herramientas h GROUP BY h.estanteria HAVING nUnidades>15;
  
-- 27 Cual es la estantería que tiene más unidades?
                           
  
-- 28 a partir de la tabla emple y depart mostrar los datos del departamento que no tiene ningún empleado
  SELECT d.dept_no, d.dnombre, d.loc FROM depart d LEFT JOIN emple e ON d.dept_no = e.dept_no WHERE e.dept_no IS NULL;
  
-- 29 mostrar el número de empleados de cada departamento.
--  En la salida se debe mostrar también los departamentos que no tienen ningún empleado
 SELECT d.dept_no numerodedepartamento, e.emp_no numerodempleados FROM depart d LEFT JOIN emple e 
 ON d.dept_no = e.dept_no GROUP BY d.dept_no;

-- 30 obtener la suma de salarios de cada departamento, mostrando las columnas ndepart, suma salarios y dnombre.
  -- En el resultado también se deben mostrar los departamentos que no tienen empleados
  SELECT d.dept_no numerodepartamento, d.dnombre, SUM( e.salario) sumadesalarios  FROM depart d LEFT JOIN emple e 
  ON d.dept_no = e.dept_no GROUP BY d.dept_no ; 
  
-- 31 Utilizar la función IFNULL en la consulta anterior para que en el caso que un departamento no tenga empleados,
  -- aparezca como suma de salarios el valor 0
  
  
-- 32 Obtener el número de médicos que pertenecen a cada hospital, mostrando las columnas COD_HOSPITAL, NOMBRE y NUMERO DE MEDICOS.
  -- En el resultado deben aparecer también los datos de los hospitales que no tienen médicos                 